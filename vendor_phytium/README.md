# vendor_phytium

## 介绍

该仓库对应phytium硬件产品d2000。

## 目录

```
vendor/phytium
├── d2000
│   ├── audio
│   ├── bluetooth
│   ├── config.json
│   ├── hals
│   ├── hdf_config
│   ├── ohos.build
│   ├── preinstall-config
│   ├── product.gni
│   └── resourceschedule
└── ...
```

## 相关仓

* [device/board/phytium](https://gitee.com/phytium_embedded/phytium-openharmony-d2000-device/tree/master/device_board_phytium)
* [device/soc/phytium](https://gitee.com/phytium_embedded/phytium-openharmony-d2000-device/tree/master/device_soc_phytium)

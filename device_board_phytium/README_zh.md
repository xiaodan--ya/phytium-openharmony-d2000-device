# device_board_phytium

## 简介

### D2000 DEV开发板简介

D2000 DEV开发板是由飞腾自研提供的基于高性能桌面芯片D2000芯片的开发板。


## 目录

```
device/board/phytium
├── d2000
│   ├── BUILD.gn
│   ├── cfg
│   ├── device.gni
│   ├── distributedhardware
│   ├── kernel
│   ├── loader
│   ├── ohos.build
│   ├── patch
│   └── updater
└── ...
```

## 使用说明

d2000使用说明参考：
- [d2000](https://gitee.com/phytium_embedded/phytium-openharmony-d2000-device/tree/master/README.md)

## 相关仓

* [vendor/phytium](https://gitee.com/phytium_embedded/phytium-openharmony-d2000-device/tree/master/vendor_phytium)
* [device/soc/phytium](https://gitee.com/phytium_embedded/phytium-openharmony-d2000-device/tree/master/device_soc_phytium)
